using LinearAlgebra
using Plots
using ProgressMeter
using LaTeXStrings
using StatsBase
using LsqFit
include("variational_monte_carlo.jl")

M = 300         # Number of Walkers
eq_steps = 10000
Δt = 0.2
α = 0.18
β = 0.38
κ = 1.85


function simulation()
    configurations = initialize_configuration(M)
    radius = zeros(Float64, 0)
    distance = zeros(Float64, 0)

    @showprogress for _ in 1:eq_steps
        configurations = fokker_planck_vmc_step.(configurations, Δt, α, β, κ)
    end

    for conf in configurations
        push!(radius, norm(conf[1]))
        push!(radius, norm(conf[2]))
        push!(distance, norm(conf[1] - conf[2]))
    end

    return radius, distance
end


res = simulation()


# Positions
h = fit(Histogram, res[1], nbins=60)
h = normalize(h, mode=:pdf)

x = h.edges[1][1:end-1]
x_fit = LinRange(x[1], x[end], 100)

gaussian(x, μ, σ) = exp(-(x - μ)^2 / 2σ^2) / (√(2π) * σ)
@. model1(x, p) = gaussian(x, p[1], p[2])
p0 = [0.4, 0.3]
fit_model = curve_fit(model1, x, h.weights, p0)
display(coef(fit_model))

plt1 = plot(h, xlabel=L"r", ylabel="normalized count", label="simulation")
plot!(x_fit, model1(x_fit, coef(fit_model)), label="gaussian fit")

savefig("plots/1h_pos.png")
display(plt1)


# Distance between
h = fit(Histogram, res[2], nbins=60)
h = normalize(h, mode=:pdf)

x = h.edges[1][1:end-1]
x_fit = LinRange(x[1], x[end], 100)

@. model2(x, p) = gaussian(x, p[1], p[2])
p0 = [2.0, 2.0]
fit_model = curve_fit(model2, x, h.weights, p0)
display(coef(fit_model))

plt2 = plot(h, xlabel=L"r_{12}", ylabel="normalized count", label="simulation")
plot!(x_fit, model2(x_fit, coef(fit_model)), label="gaussian fit")

savefig("plots/1h_dist.png")
display(plt2)
