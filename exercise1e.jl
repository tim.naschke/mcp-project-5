using Statistics
using Optim
include("variational_monte_carlo.jl")

M = 300         # Number of Walkers
steps = 10000
eq_steps = 3000
s = 1


function simulation(args)
    α = args[1]
    β = args[2]
    κ = args[3]

    configurations = initialize_configuration(M)
    local_energies = zeros(M)

    for _ in 1:eq_steps
        configurations = monte_carlo_step.(configurations, s, α, β, κ)
    end

    for _ in eq_steps+1:steps
        configurations = monte_carlo_step.(configurations, s, α, β, κ)
        @. local_energies += local_energy(configurations, α, β, κ)
    end

    local_energies ./= (steps - eq_steps - 1)

    return mean(local_energies)
end


@time res = optimize(simulation, [0.18, 0.38, 1.85])

display(Optim.minimizer(res))
display(Optim.minimum(res))
display(Optim.iterations(res))
display(Optim.converged(res))
