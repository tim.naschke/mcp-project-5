using Plots
using ProgressMeter
include("path_integral.jl")


T(x, Δτ) = sum( 0.5 * ((x[2:end] - x[1:end-1]) / Δτ).^2 )
V(x) = sum( (x[2:end] + x[1:end-1]).^2 / 8 )
S(x, Δτ) = T(x, Δτ) + V(x)
x₀(N) = zeros(N)


steps = 500000

τ₀ = 0
τ = 100
N = 400
Δx = 0.1
Δτ = (τ - τ₀) / N


function run()
    x = x₀(N + 1)
    x[1] = 0
    x[end] = 0

    e_kin = [T(x, Δτ)]
    e_pot = [V(x)]

    @showprogress for _ in 1:steps
        x = metropolis_step(x, S, Δτ, Δx)
        push!(e_kin, T(x, Δτ))
        push!(e_pot, V(x))
    end

    return e_kin, e_pot, x
end


res = run()


plt = plot(
    dpi = 600,
    legend = :right,
)
plot!(res[1], label="T")
plot!(res[2], label="V")
vline!([90000], color="black", label="Equilibration time")

savefig("plots/3a.png")
display(plt)

plot(res[3], label="x")
