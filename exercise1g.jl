using Statistics
using Plots
using ProgressMeter
using LaTeXStrings
using Measurements
include("variational_monte_carlo.jl")

M = 300         # Number of Walkers
steps = 10000
eq_steps = 4000
Δt_eval = [0.01, 0.05, 0.1, 0.2, 1]
α = 0.18
β = 0.38
κ = 1.85


function simulation(Δt, progress)
    configurations = initialize_configuration(M)
    local_energies = zeros(M)

    for _ in 1:eq_steps
        configurations = fokker_planck_vmc_step.(configurations, Δt, α, β, κ)
        next!(progress)
    end

    for _ in eq_steps+1:steps
        configurations = fokker_planck_vmc_step.(configurations, Δt, α, β, κ)
        @. local_energies += local_energy(configurations, α, β, κ)
        next!(progress)
    end

    local_energies ./= (steps - eq_steps - 1)

    return mean(local_energies), √(var(local_energies) / (M - 1))
end


function run()
    local_energies_mean = zeros(length(Δt_eval))
    local_energies_std = zeros(length(Δt_eval))

    progress = Progress(length(Δt_eval) * steps)
    Threads.@threads for (i, Δt) in collect(enumerate(Δt_eval))
        res = simulation(Δt, progress)
        local_energies_mean[i] = res[1]
        local_energies_std[i] = res[2]
    end

    return local_energies_mean, local_energies_std
end


res = run()


plt1 = scatter(
    Δt_eval,
    res[1] .± res[2],
    xlabel = L"Δt",
    ylabel = L"\langle \bar{E}_L \rangle",
    dpi = 600,
    legend = :none
)
plt2 = scatter(
    Δt_eval,
    res[2],
    xlabel = L"Δt",
    ylabel = L"σ_{\bar{E}_L}",
    dpi = 600,
    legend = :none
)
plt = plot(plt1, plt2, size=(600, 300))

savefig("plots/1g.png")
display(plt)

min_index = argmin(res[2])
println("minimum sigma:\nE = $(res[1][min_index]) +- $(res[2][min_index]) at tau = $(Δt_eval[min_index])")
