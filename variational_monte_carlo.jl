using LinearAlgebra

function trial_function(configuration::Vector{Vector{Float64}}, α, β, κ)
    r₁₂ = norm(configuration[1] - configuration[2])
    r₁ = norm(configuration[1])
    r₂ = norm(configuration[2])

    return exp(-κ * (r₁ + r₂) + β * r₁₂ / (1 + α * r₁₂))
end

function local_energy(configuration::Vector{Vector{Float64}}, α, β, κ)
    r₁ = configuration[1]
    r₂ = configuration[2]
    r₁₂ = norm(r₁ - r₂)

    u = 1 + α * r₁₂
    return (κ - 2)/norm(r₁) + (κ - 2)/norm(r₂) + (1 - 2β/u^2)/r₁₂ + 2β * α / u^3 - κ^2 - β^2/u^4 + κ * β / u^2 * (r₁ / norm(r₁) - r₂ / norm(r₂)) · (r₁ - r₂)/r₁₂
end

function quantum_force(configuration::Vector{Vector{Float64}}, α, β, κ)
    f₁ = zeros(3)
    f₂ = zeros(3)

    r₁ = configuration[1]
    r₂ = configuration[2]
    r₁₂ = norm(r₁ - r₂)
    u = 1 + α * r₁₂

    f₁ = 2 * (-κ/norm(r₁) * r₁ + β * (1/(u * r₁₂) - α/u^2) * (r₁ - r₂))
    f₂ = 2 * (-κ/norm(r₂) * r₂ - β * (1/(u * r₁₂) - α/u^2) * (r₁ - r₂))

    return [f₁, f₂]
end

function monte_carlo_step(configuration::Vector{Vector{Float64}}, s, α, β, κ)
    old_configuration = copy(configuration)
    configuration[rand(1:2)] += rand(3) * s .- s/2

    transition_probability = trial_function(configuration, α, β, κ)^2 / trial_function(old_configuration, α, β, κ)^2

    if transition_probability >= 1
        return configuration
    elseif transition_probability > rand()
        return configuration
    else
        return old_configuration
    end
end

function fokker_planck_vmc_step(configuration::Vector{Vector{Float64}}, Δt, α, β, κ)
    old_configuration = copy(configuration)
    configuration += quantum_force(configuration, α, β, κ) * Δt/2 + [randn(3), randn(3)] * √Δt

    transition_probability = greens_function(configuration, old_configuration, Δt, α, β, κ) * trial_function(configuration, α, β, κ)^2 / (greens_function(old_configuration, configuration, Δt, α, β, κ) * trial_function(old_configuration, α, β, κ)^2)

    if transition_probability >= 1
        return configuration
    elseif transition_probability > rand()
        return configuration
    else
        return old_configuration
    end
end

function greens_function(configuration::Vector{Vector{Float64}}, old_configuration::Vector{Vector{Float64}}, Δt, α, β, κ)
    return (2π * Δt)^-3 * exp(-norm(configuration - old_configuration - quantum_force(old_configuration, α, β, κ) * Δt/2)^2 / 2Δt)
end

function initialize_configuration(size=1)
    if size == 1
        return [rand(3) .- 1/2, rand(3) .- 1/2]
    end

    return [[rand(3) .- 1/2, rand(3) .- 1/2] for _ in 1:size]
end
