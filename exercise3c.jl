using Plots
using ProgressMeter
using LinearAlgebra
include("path_integral.jl")


T(x, Δτ) = sum( 0.5 * (norm.(x[2:end] - x[1:end-1]) / Δτ).^2 )
V(x) = sum( (x[2:end, 1] + x[1:end-1, 1]).^2 + 10^2 * (x[2:end, 2] + x[1:end-1, 2]).^2 ) / 8
S(x, Δτ) = T(x, Δτ) + V(x)
x₀(N) = zeros(N, 2)


steps = 500000
eq_steps = 90000

τ₀ = 0
τ = 100
N = 400
Δx = 0.1
Δτ = (τ - τ₀) / N


function runA()
    x = x₀(N + 1)
    x[1, :] = [0, 0]
    x[end, :] = [0, 0]

    e_kin = [T(x, Δτ)]
    e_pot = [V(x)]

    @showprogress for _ in 1:steps
        x = metropolis_step_2D(x, S, Δτ, Δx)
        push!(e_kin, T(x, Δτ))
        push!(e_pot, V(x))
    end

    return e_kin, e_pot
end


function evalA(res)
    plt = plot(
        dpi = 600#,
        #legend = :outertop,
        #legend_columns = -1
    )
    plot!(res[1], label="T")
    plot!(res[2], label="V")
    vline!([eq_steps], color="black", label="Equilibration time")

    savefig("plots/3c_a.png")
    display(plt)
end


function runB()
    x = x₀(N + 1)
    x[1, :] = [0, 0]
    x[end, :] = [0, 0]

    x_changed = zeros(steps - eq_steps, 2)
    
    progress = Progress(steps)
    for _ in 1:eq_steps
        x = metropolis_step_2D(x, S, Δτ, Δx)
        next!(progress)
    end

    for i in eq_steps+1:steps
        x, change_index = metropolis_step_2D(x, S, Δτ, Δx, true)
        x_changed[i - eq_steps, :] = x[change_index, :]
        next!(progress)
    end

    return x_changed
end


function evalB(x_changed)
    lims = (min(x_changed...), max(x_changed...))
    
    display(histogram2d(
        x_changed[:, 1],
        x_changed[:, 2],
        bins = (100, 100),
        aspect_ratio = :equal,
        normalize = :pdf,
        xlims = lims,
        ylims = lims
    ))
    savefig("plots/3c_b.png")
end


res = runA()
evalA(res)


x_changed = runB()
evalB(x_changed)
