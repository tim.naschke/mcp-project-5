using Statistics
using Plots
using ProgressMeter
using Measurements
using LaTeXStrings
include("variational_monte_carlo.jl")

M = 300         # Number of Walkers
steps = 30000
n = 1000        # Number of Timesteps to average over
s_eval = [0.1, 1, 10]
α = 0.15
β = 1/2
κ = 2


function run(s)
   configurations = initialize_configuration(M)
    local_energies = zeros(n, M)
    local_energy_mean = []
    local_energy_std = []

    @showprogress for i in 1:steps
        configurations = monte_carlo_step.(configurations, s, α, β, κ)
        local_energies = circshift(local_energies, -1)
        local_energies[end, :] = local_energy.(configurations, α, β, κ)

        if i % n == 0
            local_energy_time_average = mean(local_energies, dims=1)
            push!(local_energy_mean, mean(local_energy_time_average))
            push!(local_energy_std, √(var(local_energy_time_average) / (M - 1)))
        end
    end

    return local_energy_mean, local_energy_std
end


#results = run.(s_eval)


plt = plot(
    xlabel = L"t",
    ylabel = L"\langle E_L^{1000} \rangle",
    dpi = 600
)

for (i, res) in enumerate(results)
    plot!(n:n:steps, res[1], ribbon=res[2], fillalpha=0.3, label=L"s = %$(s_eval[i])")
    println("sigma_end for s = $(s_eval[i])\t: $(res[2][end])")
end

savefig("plots/1a.png")
display(plt)
