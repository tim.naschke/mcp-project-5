using Plots
using ProgressMeter
using LaTeXStrings
include("path_integral.jl")


T(x, Δτ) = sum( 0.5 * ((x[2:end] - x[1:end-1]) / Δτ).^2 )
V(x) = sum( (x[2:end] + x[1:end-1]).^2 / 8 )
S(x, Δτ) = T(x, Δτ) + V(x)
x₀(N) = zeros(N)


steps = 500000
eq_steps = 90000

τ₀ = 0
τ = 100
N = 400
Δx = 0.1
Δτ = (τ - τ₀) / N


function run()
    x = x₀(N + 1)
    x[1] = 0
    x[end] = 0

    x_changed = []
    
    progress = Progress(steps)
    for _ in 1:eq_steps
        x = metropolis_step(x, S, Δτ, Δx)
        next!(progress)
    end

    for _ in eq_steps+1:steps
        x, change_index = metropolis_step(x, S, Δτ, Δx, true)
        push!(x_changed, x[change_index])
        next!(progress)
    end

    return x_changed
end


x_changed = run()


bins = minimum(x_changed) - Δx/2 : Δx : maximum(x_changed) + Δx/2
plt = histogram(x_changed, normalize=:pdf, bins=bins, legend=false, dpi=600, xlabel=L"x", ylabel="normalized count")

savefig("plots/3b.png")
display(plt)
