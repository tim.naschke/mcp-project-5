using LinearAlgebra

function trial_function(configuration::Vector{Vector{Float64}}, α, β, κ)
    r₁₂ = norm(configuration[1] - configuration[2])
    r₁ = norm(configuration[1])
    r₂ = norm(configuration[2])

    return exp(-κ * (r₁ + r₂) + β * r₁₂ / (1 + α * r₁₂))
end

function local_energy(configuration::Vector{Vector{Float64}}, α, β, κ)
    r₁ = configuration[1]
    r₂ = configuration[2]
    r₁₂ = norm(r₁ - r₂)

    u = 1 + α * r₁₂
    return (κ - 2)/norm(r₁) + (κ - 2)/norm(r₂) + (1 - 2β/u^2)/r₁₂ + 2β * α / u^3 - κ^2 - β^2/u^4 + κ * β / u^2 * (r₁ / norm(r₁) - r₂ / norm(r₂)) · (r₁ - r₂)/r₁₂
end

function quantum_force(configuration::Vector{Vector{Float64}}, α, β, κ)
    f₁ = zeros(3)
    f₂ = zeros(3)

    r₁ = configuration[1]
    r₂ = configuration[2]
    r₁₂ = norm(r₁ - r₂)
    u = 1 + α * r₁₂

    f₁ = 2 * (-κ/norm(r₁) * r₁ + β * (1/(u * r₁₂) - α/u^2) * (r₁ - r₂))
    f₂ = 2 * (-κ/norm(r₂) * r₂ - β * (1/(u * r₁₂) - α/u^2) * (r₁ - r₂))

    return [f₁, f₂]
end

function fokker_planck_dmc_step(configuration::Vector{Vector{Vector{Float64}}}, Δt, Eₜ, α, β, κ)
    old_configuration = copy(configuration)
    result = Vector{Vector{Float64}}[]

    M = length(configuration)
    accept = zeros(M)
    q = zeros(M)
    m = zeros(M)

    diffusion = [[randn(3), randn(3)] for _ in 1:M] * √Δt
    @. configuration += quantum_force(configuration, α, β, κ) * Δt/2 + diffusion
    accept = rand(M) .< transition_probability.(configuration, old_configuration, Δt, α, β, κ)

    append!(result, old_configuration[.!accept])

    @. q[accept] = exp(-Δt * (local_energy(configuration[accept], α, β, κ) - Eₜ))

    #push!(result, configuration[accept][q[accept] .<= 1 && rand(sum(accept)) .< q[accept]])
    append!(result, configuration[q .<= 1 .&& rand(M) .< q])

    m[q .> 1] .= floor.(q[q .> 1] .+ rand(sum([q .> 1])))

    for i in 1:M
        for _ in 1:m[i]
            push!(result, configuration[i])
        end
    end

    return result
end

function greens_function(configuration::Vector{Vector{Float64}}, old_configuration::Vector{Vector{Float64}}, Δt, α, β, κ)
    return (2π * Δt)^-3 * exp(-norm(configuration - old_configuration - quantum_force(old_configuration, α, β, κ) * Δt/2)^2 / 2Δt)
end

function initialize_configuration(size)
    return [[rand(3) .- 1/2, rand(3) .- 1/2] for _ in 1:size]
end

function transition_probability(configuration, old_configuration, Δt, α, β, κ)
    return greens_function(configuration, old_configuration, Δt, α, β, κ) * trial_function(configuration, α, β, κ)^2 / (greens_function(old_configuration, configuration, Δt, α, β, κ) * trial_function(old_configuration, α, β, κ)^2)
end