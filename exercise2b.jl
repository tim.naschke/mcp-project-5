using ProgressMeter
using Plots
using Statistics
include("diffusion_monte_carlo.jl")

M = 300
N = 30000
eq_steps = 10000

Δt = 0.03
E₀ = -2.9

α = 0.18
β = 0.5
κ = 2


function run()
    configuration = initialize_configuration(M)
    energy = []
    number_walkers = []

    progress = Progress(N)
    for _ in 1:eq_steps
        Eₜ = E₀ + log(M / length(configuration))
        configuration = fokker_planck_dmc_step(configuration, Δt, Eₜ, α, β, κ)

        next!(progress)
    end

    for _ in eq_steps+1:N
        Eₜ = E₀ + log(M / length(configuration))
        configuration = fokker_planck_dmc_step(configuration, Δt, Eₜ, α, β, κ)

        push!(energy, Eₜ)
        push!(number_walkers, length(configuration))

        next!(progress)
    end

    return energy, number_walkers
end


res = run()


plt1 = plot(res[1],
    xlabel = "steps",
    ylabel = "energy",
    dpi = 600,
    label = "trial energy"
)
hline!([mean(res[1])], label="mean energy")

display("$(mean(res[1])) +- $(std(res[1]))")

savefig("plots/2b_energy.png")
display(plt1)

plt2 = plot(res[2],
    xlabel = "steps",
    ylabel = "number of walkers",
    dpi = 600,
    legend = false
)

savefig("plots/2b_walkers.png")
display(plt2)
