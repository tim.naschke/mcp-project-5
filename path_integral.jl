function metropolis_step(x, S, Δτ, Δx, return_changed=false)
    x_new = copy(x)

    change_index = rand(2:length(x)-1)
    x_new[change_index] += (1 - 2 * rand()) * Δx
    x_new[change_index] = round(x_new[change_index] / Δx) * Δx

    ΔS = S(x_new, Δτ) - S(x, Δτ)
    P = exp(-Δτ * ΔS)

    if rand() < P
        return return_changed ? (x_new, change_index) : x_new
    else
        return return_changed ? (x, change_index) : x
    end
end

function metropolis_step_2D(x, S, Δτ, Δx, return_changed=false)
    x_new = copy(x)

    change_index = rand(2:size(x)[1]-1)
    x_new[change_index, :] += (1 .- 2 * rand(2)) * Δx
    #x_new[change_index, :] = round.(x_new[change_index, :] / Δx) * Δx

    ΔS = S(x_new, Δτ) - S(x, Δτ)
    P = exp(-Δτ * ΔS)

    if rand() < P
        return return_changed ? (x_new, change_index) : x_new
    else
        return return_changed ? (x, change_index) : x
    end
end
