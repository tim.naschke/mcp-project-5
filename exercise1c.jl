using Statistics
using Plots
using ProgressMeter
using LaTeXStrings
using Measurements
using LsqFit
include("variational_monte_carlo.jl")

M = 300         # Number of Walkers
steps = 10000
eq_steps = 4000
s = 1
α_eval = 0:0.01:0.5
β = 1/2
κ = 2


function simulation(α)
    configurations = initialize_configuration(M)
    local_energies = zeros(M)

    for _ in 1:eq_steps
        configurations = monte_carlo_step.(configurations, s, α, β, κ)
    end

    for _ in eq_steps+1:steps
        configurations = monte_carlo_step.(configurations, s, α, β, κ)
        @. local_energies += local_energy(configurations, α, β, κ)
    end

    local_energies ./= (steps - eq_steps - 1)

    return mean(local_energies), √(var(local_energies) / (M - 1))
end


function run()
    local_energies_mean = zeros(length(α_eval))
    local_energies_std = zeros(length(α_eval))

    p = Progress(length(α_eval))
    Threads.@threads for (i, α) in collect(enumerate(α_eval))
        res = simulation(α)
        local_energies_mean[i] = res[1]
        local_energies_std[i] = res[2]

        next!(p)
    end

    return local_energies_mean, local_energies_std
end


res = run()


# Average
@. model(x, p) = p[1] + p[2] * x^1 + p[3] * x^2 + p[4] * x^3
p0 = zeros(4)
fit = curve_fit(model, α_eval, res[1], p0)

y_fit = model(α_eval, coef(fit))
min_index = argmin(y_fit)

println("Fit Parameters:")
display(coef(fit))
println("E_min = $(res[1][min_index]) +- $(res[2][min_index]) at α = $(α_eval[min_index])")

plt1 = plot(
    xlabel = L"α",
    ylabel = L"\langle \bar{E}_L \rangle",
    dpi = 600
)
scatter!(α_eval, res[1] .± res[2], label="simulation")
plot!(α_eval, y_fit, label="polynomial fit of order 3")
vline!([α_eval[min_index]], color="black", linestyle=:dash, label=L"$α_{\mathrm{min}}$ according to fit")

savefig("plots/1c_mean.png")
display(plt1)


# Standard Error
p0 = zeros(4)
fit = curve_fit(model, α_eval, res[2], p0)

y_fit = model(α_eval, coef(fit))
min_index = argmin(y_fit)

println("Fit Parameters:")
display(coef(fit))
println("sigma_min = $(res[2][min_index]) at α = $(α_eval[min_index])")

plt2 = plot(
    xlabel = L"α",
    ylabel = L"σ_{\langle \bar{E}_L \rangle}",
    dpi = 600
)
scatter!(α_eval, res[2], label="simulation")
plot!(α_eval, y_fit, label="polynomial fit of order 3")
vline!([α_eval[min_index]], color="black", linestyle=:dash, label=L"$α_{\mathrm{min}}$ according to fit")

savefig("plots/1c_std.png")
display(plt2)
