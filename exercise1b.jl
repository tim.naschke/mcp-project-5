using Statistics
using Plots
using ProgressMeter
using Measurements
using LaTeXStrings
include("variational_monte_carlo.jl")

M = 300         # Number of Walkers
steps = 10000
n = 1000        # Number of Timesteps to average over
s = 1
α_eval = [0, 0.1, 0.2, 0.3, 0.4, 0.5]
β = 1/2
κ = 2


function run(α)
    configurations = initialize_configuration(M)
    local_energies = zeros(n, M)
    local_energy_mean = []
    local_energy_std = []

    @showprogress for i in 1:steps
        configurations = monte_carlo_step.(configurations, s, α, β, κ)
        local_energies = circshift(local_energies, -1)
        local_energies[end, :] = local_energy.(configurations, α, β, κ)

        if i % n == 0
            local_energy_time_average = mean(local_energies, dims=1)
            push!(local_energy_mean, mean(local_energy_time_average))
            push!(local_energy_std, √(var(local_energy_time_average) / (M - 1)))
        end
    end

    return local_energy_mean, local_energy_std
end


results = run.(α_eval)


plt = plot(
    xlabel = L"t",
    ylabel = L"\langle E_L^{%$n} \rangle",
    dpi = 600,
    legend_position = :right
)

for (i, res) in enumerate(results)
    plot!(n:n:steps, res[1], label=L"α = %$(α_eval[i])")
end

vline!([4000], color="black", linestyle=:dash, label="equilibration time")

savefig("plots/1b.png")
display(plt)
